import { autoUpdater } from "electron-updater";
const { app, BrowserWindow, ipcMain, dialog, electron } = require('electron');
const path = require('path');
const log = require('electron-log');
autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 600,
    width: 1220,
    resizable: true
  })

  mainWindow.loadURL('http://localhost/dist')

  mainWindow.on('closed', () => {
    mainWindow = null
  })
  ipcMain.once('check-for-update', function (event, arg) {
    log.info(app.getVersion())
    updateHandle()
  })
}
app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

// 检测更新，在你想要检查更新的时候执行，renderer事件触发后的操作自行编写
function updateHandle() {
  //执行自动更新检查
  autoUpdater.checkForUpdates();

  let message = {
    error: { msg: '检查更新出错,请联系管理人员', code: 0 },
    checking: { msg: '正在检查更新……', code: 1 },
    updateAva: { msg: '检测到新版本，正在下载……', code: 2 },
    updateNotAva: { msg: '当前为最新版本', code: 3 },
    downloaded:{msg:'下载完成，即将更新软件', code:4}
  };
  autoUpdater.setFeedURL('http://my-1253757538.cosgz.myqcloud.com/');

  autoUpdater.on('error', function () {
    sendUpdateMessage(message.error)
  });

  //当开始检查更新的时候触发
  autoUpdater.on('checking-for-update', function () {
    sendUpdateMessage(message.checking)
  });

  //当发现一个可用更新的时候触发，更新包下载会自动开始
  autoUpdater.on('update-available', function () {
    sendUpdateMessage(message.updateAva)
  });

  //当没有可用更新的时候触发
  autoUpdater.on('update-not-available', function () {
    sendUpdateMessage(message.updateNotAva)
  });

  // 更新下载进度事件
  autoUpdater.on('download-progress', function (progressObj) {
    mainWindow.webContents.send('downloadProgress', progressObj)
  })

  //下载完成
  autoUpdater.on('update-downloaded', function (event, releaseNotes, releaseName, releaseDate, updateUrl, quitAndUpdate) {
    // var index = dialog.showMessageBox(mainWindow, {
    //   type: 'info',
    //   buttons: ['现在重启', '稍后重启'],
    //   message: message.downloaded.msg,
    //   //detail: releaseName + "\n\n" + releaseNotes
    // });
    // console.log(index);
    // if (index === 1) {
    //   mainWindow.webContents.send('had-downloaded');
    //   return;
    // }

    //通过main进程发送事件给renderer进程，提示更新信息
    // mainWindow.webContents.send('isUpdateNow')

    //在下载完成后，重启当前的应用并且安装更新
    autoUpdater.quitAndInstall();
  });
}

// 通过main进程发送事件给renderer进程，提示更新信息
// mainWindow = new BrowserWindow()
function sendUpdateMessage(text) {
  log.info(text);
  mainWindow.webContents.send('message', text)
}

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
