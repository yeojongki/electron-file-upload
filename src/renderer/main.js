import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import '@/assets/iconfont/iconfont.js'
import IconSvg from '@/components/IconSvg'
import MyLocalStorage from '@/utils/MyLocalStorage'

Vue.use(ElementUI)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.component('icon-svg', IconSvg)

const whiteList = ['/login']
router.beforeEach((to, from, next) =>{
  if (MyLocalStorage.get('zxm')){
    //存在token
    if (to.path === '/login') {
      next({ path: '/' })
    } else {
      if (store.getters.roles.length === 0) {
        store.dispatch('GetInfo')
      }
      if(!store.getters.oss) {
        store.dispatch('GetOss')
      }
      next()
    }
  }else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    }else {
      next('/login')
    }
  }
})
/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
