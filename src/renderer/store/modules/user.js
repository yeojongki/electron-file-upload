import { login, logout, getInfo, getOss } from '@/api/login';
import { Message, MessageBox } from 'element-ui';
import MyLocalStorage from '@/utils/MyLocalStorage'
let token = null;
let storage = MyLocalStorage.get('zxm')
if (storage) {
  token = storage.token
}

const user = {
  state: {
    token: token,
    name: '',
    avatar: null,
    roles: [],
    tel: '',
    oss: null
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_NAME: (state, name) => {
      state.name = name;
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar;
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles;
    },
    SET_TEL: (state, tel) => {
      state.tel = tel;
    },
    SET_OSS: (state, oss) => {
      state.oss = oss;
    },
  },

  actions: {
    // 登录
    //alert: 权限认证，写了固定值
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim();
      return new Promise((resolve, reject) => {
        login(username, userInfo.passwd).then(res => {
          if (res.code == 1) {
            const data = res.data;
            commit('SET_NAME', data.userinfo.nicename);
            commit('SET_AVATAR', data.userinfo.avatar);
            let userData = {
              token: data.token
            };

            //storage
            MyLocalStorage.set('zxm',userData, 60*60*2)//2h
            commit('SET_TOKEN', data.token);
            resolve(1);
          }
        }).catch(error => {
          reject(error);
        });
      });
    },
    // 获取用户信息
    //alert: 权限认证，写了固定值，
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(res => {
          if (res.code == 1) {
            const data = res.data;
            commit('SET_ROLES', ["admin"]);
            commit('SET_NAME', data.nicename);
            commit('SET_AVATAR', data.avatar);
            commit('SET_TEL', data.mobile);
            resolve(res);
          }
        }).catch(error => {
          reject(error);
        });
      });
    },
    //获取Oss
    GetOss({ commit, state }) {
      return new Promise((resolve, reject) => {
        getOss(state.token).then(res => {
          if (res.code == 1) {
            commit('SET_OSS', res.data);
          }
        }).catch(err => {
          reject(err);
        })
      });
    },

    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(res => {
          if(res.code == 1) {
            resolve(res);
          }
        }).catch(error => {
          reject(error);
        });
        //storage
        localStorage.removeItem('zxm')
        commit('SET_TOKEN', '');
        commit('SET_ROLES', []);
      });
    }

  }
};

export default user;