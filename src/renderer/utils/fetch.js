import axios from 'axios';
import { Message, MessageBox } from 'element-ui';
import store from '../store';

let baseUrl;
if (process.env.NODE_ENV == 'development') {
  baseUrl = 'https://www.cyan-net.com/v10/'
}else {
  baseUrl = 'https://www.cyan-net.com/v10/'
}
// axios.defaults.withCredentials = true;
// 创建axios实例
const service = axios.create({
  baseURL: baseUrl, // api的base_url
  timeout: 1000 * 15, // 请求超时时间
});
// request拦截器
service.interceptors.request.use(config => {
  config.headers['Content-type'] = "application/x-www-form-urlencoded";
  return config;
}, error => {
  Promise.reject(error);
})

// respone拦截器
service.interceptors.response.use(
  response => {
    const res = response.data;
    // -101 token失效 -1请求超时
    if (res.code == -101 || res.code == -1 || res.code == -98) {
      let info = res.code == -101 || res.code == -1 ? '登录超时':res.msg
      MessageBox.alert('原因:'+info, '网络出错:', {
        confirmButtonText: '确定',
        callback: action => {
          localStorage.removeItem('zxm')
          location.reload(); // 为了重新实例化vue-router对象 避免bug
        }
      });
      return
    }
    if (res.code == 0) {
      Message({
        message: res.msg,
        type: 'error',
        duration: 5 * 1000
      });
    } else {
      return response.data;
    }
  }, error => {
    if (error.message == 'Network Error' || error.message == 'timeout of 15000ms exceeded') {
      error.message = '服务器出错，请联系管理员'
    }
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    });
    return Promise.reject(error);
  }
)

export default service;