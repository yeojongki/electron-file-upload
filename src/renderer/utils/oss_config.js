require('./md5.js')
const co = require('co')
const OSS = require('ali-oss')

export function config_oss(oss) {
  const client = new OSS({
    endpoint: 'https://' + oss.endpoint,
    accessKeyId: oss.KeyId,
    accessKeySecret: oss.KeySecret,
    bucket: oss.bucket,
    secure: true
  });
  return client
}

export async function oss_upload(oss, files) {

}