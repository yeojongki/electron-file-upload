var qs = require('querystring')
import fetch from '@/utils/fetch';

export function getOss(token) {
  let time = parseInt((new Date()).valueOf() / 1000);
  const data = {
    time:time,
    token:token,
    act: "oss"
  };
  return fetch({
    url: '/shop',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function login(username, passwd, vcode) {
  let time = parseInt((new Date()).valueOf() / 1000);
  const data = {
    username: username,
    passwd: passwd,
    time: time
  };
  return fetch({
    url: '/login',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function getInfo(token) {
  let time = parseInt((new Date()).valueOf() / 1000);
  const data = {
    time,
    token,
    act: "info"
  };
  return fetch({
    url: '/user',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function logout(token) {
  let time = parseInt((new Date()).valueOf() / 1000);  
  const data = {
    time,
    token,
    act: "logout"
  };
  return fetch({
    url: '/user',
    method: 'post',
    data: qs.stringify(data)    
  });
}