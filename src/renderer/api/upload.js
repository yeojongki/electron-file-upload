const qs = require('querystring')
import fetch from '@/utils/fetch';
import axios from 'axios'
export function uploadFiles(token, oj) {
  let time = parseInt((new Date()).valueOf() / 1000);
  const data = {
    time:time,
    token:token,
    act: "oss",
    oj
  };
  return fetch({
    url: '/shop',
    method: 'post',
    data: qs.stringify(data)
  });
}

export function checkFiles() {
  return axios.post('http://www.easy-mock.com/mock/59a0395a8c614f3140d4cf5d/data/list')
}